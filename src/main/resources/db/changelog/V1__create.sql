--liquibase formatted sql

--changeset nick:create

create table teacher (
    id serial unique,
    fio         varchar(50),
    kafedra varchar(30)
);

create table student (
    id serial,
    fio         varchar(50),
    specialnost varchar(30),
    kurs        int,
    teacher_id int references teacher (id)
);

