package com.example.day11.Controller;

import com.example.day11.DTO.StudentDTO;
import com.example.day11.Model.Student;
import com.example.day11.Service.api.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("student")
public class StudentController {

    @Autowired
    ProduceMessageController pmc;

    @Autowired
    private StudentService studentService;

    @PostMapping("/save")
    public void save(@RequestBody List<StudentDTO> DTO) {
        for (StudentDTO e : DTO) {
            studentService.save(e);
            pmc.sendMessage("Student saved");
        }
    }

    @PostMapping("/update")
    public void update(@RequestBody List<Student> students) {
        for (Student e : students) {
            studentService.update(e);
            pmc.sendMessage("Student updated");
        }
    }

    @PostMapping("/delete")
    public void delete(@RequestBody List<StudentDTO> DTO) {
        for (StudentDTO e : DTO) {
            studentService.delete(e.getId());
            pmc.sendMessage("Student deleted");
        }
    }

    @GetMapping("/get/{id}")
    public Student getById(@PathVariable Long id) {
        pmc.sendMessage("Student got by id");
        return studentService.get(id);
    }

    @GetMapping("/get-all")
    public List<Student> getAll() {
        pmc.sendMessage("got all students ");
        return studentService.getAll();
    }

    @GetMapping("/get-by-teacher/{id}")
    public List<Student> getAllByTeacher(@PathVariable Long id) {
        pmc.sendMessage("Student got by teacher");
        return studentService.getByTeacher(id);
    }
    
}
