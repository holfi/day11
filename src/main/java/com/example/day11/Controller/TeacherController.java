package com.example.day11.Controller;

import com.example.day11.DTO.StudentDTO;
import com.example.day11.DTO.TeacherDTO;
import com.example.day11.Model.Student;
import com.example.day11.Model.Teacher;
import com.example.day11.Service.api.StudentService;
import com.example.day11.Service.api.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    ProduceMessageController pmc;

    @Autowired
    private TeacherService teacherService;

    @PostMapping("/save")
    public void save(@RequestBody List<TeacherDTO> DTO) {
        for (TeacherDTO e : DTO) {
            teacherService.save(e);
            pmc.sendMessage("Teacher saved");
        }
    }

    @PostMapping("/update")
    public void update(@RequestBody List<Teacher> teachers) {
        for (Teacher e : teachers) {
            teacherService.update(e);
            pmc.sendMessage("Teacher updated");
        }
    }

    @PostMapping("/delete")
    public void delete(@RequestBody List<TeacherDTO> DTO) {
        for (TeacherDTO e : DTO) {
            teacherService.delete(e.getId());
            pmc.sendMessage("Teacher deleted");
        }
    }

    @GetMapping("/get/{id}")
    public Teacher getById(@PathVariable Long id) {
        pmc.sendMessage("Teacher got by id");
        return teacherService.get(id);
    }

    @GetMapping("/get-all")
    public List<Teacher> getAll() {
        pmc.sendMessage("got all teachers");
        return teacherService.getAll();
    }

}
