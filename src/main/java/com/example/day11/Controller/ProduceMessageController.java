package com.example.day11.Controller;

import com.example.day11.Service.JmsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ProduceMessageController {

    @Autowired
    JmsProducer jmsProducer;

    @GetMapping(value="/api/message")
    public void sendMessage(String text){
        jmsProducer.sendMessage(text);
        System.out.println(text);
    }
}
