package com.example.day11;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.day11.Mapper")
public class Day11Application {

    public static void main(String[] args) {
        SpringApplication.run(Day11Application.class, args);
    }

}
