package com.example.day11.Service;

import com.example.day11.DTO.TeacherDTO;
import com.example.day11.Mapper.TeacherMapper;
import com.example.day11.Model.Teacher;
import com.example.day11.Service.api.TeacherService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    ModelMapper mapper = new ModelMapper();

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public void save(TeacherDTO save) {
        Teacher teacher = mapper.map(save, Teacher.class);
        teacherMapper.save(teacher);
    }

    @Override
    public Teacher get(Long id) {
        return teacherMapper.getById(id);
    }

    @Override
    public List<Teacher> getAll() {
        return teacherMapper.getAll();
    }

    @Override
    public void update(Teacher teacher) {
        teacherMapper.update(teacher);
    }

    @Override
    public void delete(Long id) {
        teacherMapper.delete(id);
    }
    
}
