package com.example.day11.Service.api;

import com.example.day11.DTO.StudentDTO;
import com.example.day11.Model.Student;
import com.example.day11.Model.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {

    void save(StudentDTO save);

    Student get(Long id);

    List<Student> getByTeacher(Long id);

    List<Student> getAll();

    void update(Student student);

    void delete(Long id);

}
