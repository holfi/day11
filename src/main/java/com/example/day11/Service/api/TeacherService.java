package com.example.day11.Service.api;

import com.example.day11.DTO.TeacherDTO;
import com.example.day11.Model.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeacherService {

    void save(TeacherDTO save);

    Teacher get(Long id);

    List<Teacher> getAll();

    void update(Teacher teacher);

    void delete(Long id);

}
