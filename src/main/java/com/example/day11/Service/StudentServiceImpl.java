package com.example.day11.Service;

import com.example.day11.DTO.StudentDTO;
import com.example.day11.Mapper.StudentMapper;
import com.example.day11.Model.Student;
import com.example.day11.Model.Teacher;
import com.example.day11.Service.api.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    ModelMapper mapper = new ModelMapper();

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public void save(StudentDTO save) {
        Student student = mapper.map(save, Student.class);
        studentMapper.save(student);
    }

    @Override
    public Student get(Long id) {
        return studentMapper.getById(id);
    }

    @Override
    public List<Student> getByTeacher(Long id) {
        return studentMapper.getByTeacher(id);
    }

    @Override
    public List<Student> getAll() {
        return studentMapper.getAll();
    }

    @Override
    public void update(Student student) {
        studentMapper.update(student);
    }

    @Override
    public void delete(Long id) {
        studentMapper.delete(id);
    }
}
