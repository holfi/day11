package com.example.day11.Model;

import lombok.Data;

@Data
public class Teacher {

    Long id;
    String fio;
    String kafedra;

}
