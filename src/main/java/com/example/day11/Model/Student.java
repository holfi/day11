package com.example.day11.Model;

import lombok.Data;

@Data
public class Student {

    Long id;
    String fio;
    String specialnost;
    Integer kurs;
    Integer teacherId;

}
