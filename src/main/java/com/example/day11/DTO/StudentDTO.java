package com.example.day11.DTO;

import lombok.Data;

@Data
public class StudentDTO {

    Long id;
    String fio;
    String specialnost;
    Integer kurs;
    Integer teacherId;

    public Long getId() {
        return id;
    }

    public String getFio() {
        return fio;
    }

    public String getSpecialnost() {
        return specialnost;
    }

    public Integer getKurs() {
        return kurs;
    }
}
