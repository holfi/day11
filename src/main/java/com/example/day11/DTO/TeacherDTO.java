package com.example.day11.DTO;

import lombok.Data;

@Data
public class TeacherDTO {

    Long id;
    String fio;
    String kafedra;

    public Long getId() {
        return id;
    }

    public String getFio() {
        return fio;
    }

    public String getKafedra() {
        return kafedra;
    }
}
