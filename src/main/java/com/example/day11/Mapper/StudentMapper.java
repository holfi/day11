package com.example.day11.Mapper;

import com.example.day11.Model.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface StudentMapper {

    @Select("select * from student where id = ${id}")
    Student getById(@Param("id") Long id);

    @Select("select * from student")
    List<Student> getAll();

    @Select("select * from student where teacher_id = ${id}")
    List<Student> getByTeacher(@Param("id") Long id);

    @Update("update student " +
            "set fio = #{l.fio}," +
            "specialnost = #{l.specialnost}," +
            "kurs = #{l.kurs} " +
            "where id = #{l.id}")
    void update(@Param("l") Student student);

    @Insert("insert into student(fio, specialnost, kurs, teacher_id) " +
            "values (#{l.fio}, #{l.specialnost}, #{l.kurs}, #{l.teacherId})")
    void save(@Param("l") Student student);

    @Delete("delete from student where id = ${id}")
    void delete(@Param("id") Long id);

}
