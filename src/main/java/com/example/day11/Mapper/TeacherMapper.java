package com.example.day11.Mapper;

import com.example.day11.Model.Student;
import com.example.day11.Model.Teacher;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeacherMapper {

        @Select("select * from teacher where id = ${id}")
        Teacher getById(@Param("id") Long id);

        @Select("select * from teacher")
        List<Teacher> getAll();

        @Update("update teacher " +
                "set fio = #{l.fio}," +
                "kafedra = #{l.kafedra} " +
                "where id = #{l.id}")
        void update(@Param("l") Teacher teacher);

        @Insert("insert into teacher (fio, kafedra) " +
                "values (#{l.fio}, #{l.kafedra})")
        void save(@Param("l") Teacher teacher);

        @Delete("delete from teacher where id = ${id}")
        void delete(@Param("id") Long id);

}
